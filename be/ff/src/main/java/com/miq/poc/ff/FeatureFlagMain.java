package com.miq.poc.ff;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.dropwizard.DropwizardMetricsOptions;

public class FeatureFlagMain {
	private Vertx vertx;

	public static void main(String[] args) {
		FeatureFlagMain main = new FeatureFlagMain();
		try {
			main.start();
		} catch (Exception e) {
			System.out.println("Error");
		}
	}

	public void start() throws Exception {

		FeatureFlagController ffController = new FeatureFlagController();
		vertx = Vertx.vertx(new VertxOptions().setMetricsOptions(new DropwizardMetricsOptions().setEnabled(true))
				.setWorkerPoolSize(100).setMetricsOptions(new DropwizardMetricsOptions().setEnabled(true)));
		vertx.deployVerticle(ffController, new DeploymentOptions());

		System.out.println("AMAP Service Started");
	}
}
