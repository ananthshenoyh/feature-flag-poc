package com.miq.poc.ff;

public class FeatureFlagUtil {
	static ConfigHelper configHelper = ConfigHelper.INSTANCE;

	static boolean ff1 = configHelper.getValueFromConfig("featureEnabled", Boolean.class);
	
	public static void toggleFF1() {
		ff1 = !ff1;
	}
	
	public static boolean getFF1() {
		return ff1;
	}
}
