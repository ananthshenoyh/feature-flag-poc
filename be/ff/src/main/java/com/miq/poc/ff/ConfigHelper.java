package com.miq.poc.ff;

import java.nio.file.Paths;
import java.util.Arrays;

import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.classpath.ClasspathConfigurationSource;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;

public enum ConfigHelper {

	  INSTANCE;

	  private ConfigurationProvider provider;

	  ConfigHelper() {
	    final ConfigFilesProvider configFilesProvider = () -> Arrays
	        .asList(Paths.get("test.properties"), Paths.get("flag.properties"));

	    final ConfigurationSource source = new ClasspathConfigurationSource(configFilesProvider);

	    provider = new ConfigurationProviderBuilder().withConfigurationSource(source).build();
	  }

	  public <T> T getValueFromConfig(final String key, final Class<T> type) {
	    return provider.getProperty(key, type);
	  }

}
