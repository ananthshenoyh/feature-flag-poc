package com.miq.poc.ff;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class FeatureFlagController extends AbstractVerticle {
	private FeatureFlagController that = this;
	ConfigHelper configHelper = ConfigHelper.INSTANCE;

	@Override
	public void start(Future<Void> fut) {
		System.out.println("Studio Controller Event Loop Thread: {} ");
		Router router = Router.router(vertx);
		router.route().handler(BodyHandler.create());
		router.options("/*").handler(that::optionsCall);
		router.get("/setFeature").blockingHandler(that::setFeature1);
		router.get("/r1").blockingHandler(that::testroute1);

		final Integer port = config().getInteger("http.port", 8090);
		vertx.createHttpServer().requestHandler(router::accept).listen(port, result -> {
			if (result.succeeded()) {
				fut.complete();
			} else {
				fut.fail(result.cause());
			}
		});
	}

	private void testroute1(final RoutingContext routingContext) {
		HttpServerResponse response = null;
		try {
			String val = configHelper.getValueFromConfig("prop1", String.class);
			String ff = configHelper.getValueFromConfig("featureEnabled", String.class);
			System.out.println("val " + val + " " + ff);
			response = routingContext.response().putHeader("content-type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*")
					.putHeader("Access-Control-Allow-Headers", "Content-Type")
					.putHeader("Access-Control-Allow-Headers", "Authorization")
					.putHeader("Access-Control-Allow-Methods", "GET, POST, PUT , OPTIONS");
			
			response.end("Config is: "+val+" and feature flag is: "+ff);
		} catch (Exception e) {
			System.out.println("Error in route1");
			response.end("error");
		}
	}

	private void setFeature1(final RoutingContext routingContext) {
		HttpServerResponse response = null;
		try {
			boolean ff = FeatureFlagUtil.getFF1();
			System.out.println("flag before " + ff);
			FeatureFlagUtil.toggleFF1();
			boolean ff1 = FeatureFlagUtil.getFF1();
			System.out.println("flag after " + FeatureFlagUtil.getFF1());
			response = routingContext.response().putHeader("content-type", "application/json")
					.putHeader("Access-Control-Allow-Origin", "*")
					.putHeader("Access-Control-Allow-Headers", "Content-Type")
					.putHeader("Access-Control-Allow-Headers", "Authorization")
					.putHeader("Access-Control-Allow-Methods", "GET, POST, PUT , OPTIONS");
			
			response.end("Flag before is: "+String.valueOf(ff)+" and feature flag after is: "+String.valueOf(ff1));
		} catch (Exception e) {
			System.out.println("Error in route1");
		}
	}

	private void optionsCall(final RoutingContext routingContext) {
		try {
			routingContext.response().putHeader("Access-Control-Allow-Origin", "*")
					.putHeader("Access-Control-Allow-Methods", "GET, POST, PUT , DELETE, OPTIONS")
					.putHeader("Access-Control-Allow-Headers",
							"Content-Type,cache-control, x-requested-with,Authorization")
					.putHeader("Access-Control-Max-Age", "86400").end();
		} catch (final Exception e) {
			System.out.println("Exception while setting routing context option calls.");
		}
	}

}
